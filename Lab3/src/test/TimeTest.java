package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {
	
	@Test
	public void testGetMilisecondsRegular() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetMilisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		//assertTrue("Invalid number of milliseconds", totalMilliseconds == 5151510);
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetMillisecondsBoundaryIN() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected = NumberFormatException.class)
	public void testGetMillisecondsBoundaryOUT() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of milliseconds");
	}

}