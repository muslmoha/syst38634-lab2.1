package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {

	@Test
	public void testIsPasswordLongEnoughRegular() {
		PasswordValidator pv = new PasswordValidator();
		assertTrue("Password not long enough", pv.isPasswordLongEnough("1234567890"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIsPasswordLongEnoughException() {
		PasswordValidator pv = new PasswordValidator();
		pv.isPasswordLongEnough("");
		fail("Please enter a valid password");
	}

	@Test
	public void testIsPasswordLongEnoughBoundaryIN() {
		PasswordValidator pv = new PasswordValidator();
		assertTrue("Password not long enough", pv.isPasswordLongEnough("1abscde8"));
	}

	@Test (expected = IllegalArgumentException.class)
	public void testIsPasswordLongEnoughboundaryOUT() {
		PasswordValidator pv = new PasswordValidator();
		pv.isPasswordLongEnough("abcasda1");
		fail("Please enter a valid password");
	}

	
}
