/*
 * Name: Mohammed Musleh
 * Student #: 991508115
 * The class will test whether or not a password passes 2 validations and returns a boolean.
 * 1) it must be greater than 8 characters long
 * 2) it must include at least 2 digits
 * */

package test;

public class PasswordValidator  {
	public boolean isPasswordLongEnough(String password) throws IllegalArgumentException {
		boolean isValidLength = password.length() >= 8;
		char[] passChars = password.toCharArray();
		int numberOfDigits = 0;
		for(char c : passChars) {
			if(Character.isDigit(c))
				numberOfDigits++;
		}
		boolean hasEnoughDigits = numberOfDigits >= 2;
		if(!isValidLength || !hasEnoughDigits) {
			throw new NumberFormatException("Password not long enough");
		}
		return (isValidLength && hasEnoughDigits);

	}
	
	public boolean hasUpper(String password) throws IllegalArgumentException{
		char[] passChars = password.toCharArray();
		for (char c: passChars) {
			if(Character.isUpperCase(c))
				return true;
		}
		throw new IllegalArgumentException("No uppercase found");
	}
}
