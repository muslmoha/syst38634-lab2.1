package test;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest2 {

	@Test
	public void testHasUpperRegular() {
		PasswordValidator pv = new PasswordValidator();
		assertTrue("Password has no upper case", pv.hasUpper("THIS"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void testHasUpperException() {
		PasswordValidator pv = new PasswordValidator();
		pv.hasUpper("");
		fail("Please enter a valid password");
	}
	
	@Test
	public void testHasUpperBoundaryIn() {
		PasswordValidator pv = new PasswordValidator();
		assertTrue("Password has no upper case", pv.hasUpper("hTis"));
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testHasUpperBoundaryOut() {
		PasswordValidator pv = new PasswordValidator();
		pv.hasUpper("asdasdasdsaq");
		fail("Please enter a valid password");
	}

}
